CREATE DATABASE  IF NOT EXISTS `fredagsbar` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fredagsbar`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: fredagsbar
-- ------------------------------------------------------
-- Server version	5.7.9-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attending`
--

DROP TABLE IF EXISTS `attending`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attending` (
  `fk_bar_name` varchar(25) NOT NULL,
  `fk_user_email` char(20) NOT NULL,
  `fk_datetime_from` datetime NOT NULL,
  PRIMARY KEY (`fk_bar_name`,`fk_user_email`,`fk_datetime_from`),
  KEY `fk_user_email_idx` (`fk_user_email`),
  KEY `fk_bar_name_idx` (`fk_bar_name`),
  KEY `fk_datetime_from` (`fk_datetime_from`),
  CONSTRAINT `fk_bar_name` FOREIGN KEY (`fk_bar_name`) REFERENCES `barinfo` (`bar_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_datetime_from` FOREIGN KEY (`fk_datetime_from`) REFERENCES `events` (`event_datetime_from`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_email` FOREIGN KEY (`fk_user_email`) REFERENCES `users` (`user_email`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attending`
--

LOCK TABLES `attending` WRITE;
/*!40000 ALTER TABLE `attending` DISABLE KEYS */;
INSERT INTO `attending` VALUES ('Kanalrundfartenenen','bjoe1381@stud.kea.dk','2015-12-01 01:30:00'),('Social Cluben','bjoe1381@stud.kea.dk','2015-12-07 14:49:57'),('Kanalrundfartenenen','bjoe1381@stud.kea.dk','2015-12-09 14:54:00');
/*!40000 ALTER TABLE `attending` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barfacilitet`
--

DROP TABLE IF EXISTS `barfacilitet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barfacilitet` (
  `bar_name` varchar(20) NOT NULL,
  `faci_name` varchar(45) NOT NULL,
  PRIMARY KEY (`bar_name`,`faci_name`),
  KEY `faci_name` (`faci_name`),
  CONSTRAINT `bar_name` FOREIGN KEY (`bar_name`) REFERENCES `barinfo` (`bar_name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `faci_name` FOREIGN KEY (`faci_name`) REFERENCES `faciliteter` (`faci_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barfacilitet`
--

LOCK TABLES `barfacilitet` WRITE;
/*!40000 ALTER TABLE `barfacilitet` DISABLE KEYS */;
INSERT INTO `barfacilitet` VALUES ('Kanalrundfartenenen','Beerpong'),('tacofest2000','Beerpong'),('Social Cluben','Bordfodbold'),('Kanalrundfartenenen','Bordtennis'),('tacofest2000','Bordtennis'),('Social Cluben','Udendørsareal'),('tacofest2000','Udendørsareal');
/*!40000 ALTER TABLE `barfacilitet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `barinfo`
--

DROP TABLE IF EXISTS `barinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barinfo` (
  `bar_name` varchar(25) NOT NULL,
  `bar_section` varchar(10) DEFAULT NULL,
  `bar_info` text,
  `bar_password` char(32) NOT NULL,
  PRIMARY KEY (`bar_name`),
  KEY `bar_section_idx` (`bar_section`),
  CONSTRAINT `bar_section` FOREIGN KEY (`bar_section`) REFERENCES `sections` (`sec_name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barinfo`
--

LOCK TABLES `barinfo` WRITE;
/*!40000 ALTER TABLE `barinfo` DISABLE KEYS */;
INSERT INTO `barinfo` VALUES ('Kanalrundfartenenen','Design','Pris: 20kr','5f4dcc3b5aa765d61d8327deb882cf99'),('Social Cluben','Digital','hej','d41d8cd98f00b204e9800998ecf8427e'),('tacofest2000',NULL,'King kong taco','f869ce1c8414a264bb11e14a2c8850ed');
/*!40000 ALTER TABLE `barinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `event_barname` varchar(25) NOT NULL,
  `event_info` text,
  `event_datetime_from` datetime NOT NULL,
  `event_datetime_to` datetime DEFAULT NULL,
  PRIMARY KEY (`event_datetime_from`,`event_barname`),
  KEY `event_barname_idx` (`event_barname`),
  CONSTRAINT `event_barname` FOREIGN KEY (`event_barname`) REFERENCES `barinfo` (`bar_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES ('Kanalrundfartenenen','Hyggelygge','2015-12-01 01:30:00','2015-12-02 11:30:00'),('Kanalrundfartenenen','Phasellus ut tempus turpis. Sed ornare cursus luctus. Vestibulum non fermentum nisl. Cras in sem id enim imperdiet sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sit amet elementum neque. Vivamus elementum eget sem a sodales. ...','2015-12-09 14:54:00','2015-11-11 00:00:00'),('Kanalrundfartenenen','JohnDoe aosijfasdf','2015-12-09 16:37:00','2015-12-02 00:00:00'),('Social Cluben','Huhej','2015-12-10 15:00:00','2015-11-24 12:12:00'),('Social Cluben','asdfagrhetag','2015-12-11 12:12:00','2015-12-27 11:11:00');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faciliteter`
--

DROP TABLE IF EXISTS `faciliteter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faciliteter` (
  `faci_name` varchar(40) NOT NULL,
  PRIMARY KEY (`faci_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faciliteter`
--

LOCK TABLES `faciliteter` WRITE;
/*!40000 ALTER TABLE `faciliteter` DISABLE KEYS */;
INSERT INTO `faciliteter` VALUES ('Beerpong'),('Bordfodbold'),('Bordtennis'),('Udendørsareal');
/*!40000 ALTER TABLE `faciliteter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `sec_name` varchar(20) NOT NULL,
  `sec_address` varchar(40) NOT NULL,
  PRIMARY KEY (`sec_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES ('Byg','Prinsesse Charlottes Gade 38'),('Design','Guldbergsgade 29N'),('Digital','Lygten 37'),('Teknik','Landskronagade 64');
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_name` varchar(50) DEFAULT NULL,
  `user_email` char(20) NOT NULL,
  `user_section` varchar(20) DEFAULT NULL,
  `user_linje` varchar(40) DEFAULT NULL,
  `user_rank` bit(1) DEFAULT NULL,
  `user_password` char(32) NOT NULL,
  PRIMARY KEY (`user_email`),
  KEY `user_section_idx` (`user_section`),
  CONSTRAINT `user_section` FOREIGN KEY (`user_section`) REFERENCES `sections` (`sec_name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('John Doe','bjoe1381@stud.kea.dk','Digital','Datamatiker',NULL,'5f4dcc3b5aa765d61d8327deb882cf99'),('Johnny Reimer','bjoe1382@stud.kea.dk','Byg','Tekniker','','5f4dcc3b5aa765d61d8327deb882cf99'),('Kim Johan','John1234@stud.kea.dk','Design','Datamatiker',NULL,'5f4dcc3b5aa765d61d8327deb882cf99'),('Steffen Andersen','John1334@stud.kea.dk','Teknik','dat',NULL,'5f4dcc3b5aa765d61d8327deb882cf99'),('Lars Carstensen','John2234@stud.kea.dk',NULL,'Data','','5f4dcc3b5aa765d61d8327deb882cf99');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-17  8:09:50
