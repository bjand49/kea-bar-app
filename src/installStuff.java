import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Created by B on 17-12-2015.
 */
public class installStuff
{
    public static void main(String[] args)
    {
        /**
         *
         * hyggelygge
         *
         * */
        try
        {
            String url = "jdbc:mysql://localhost:3306";
            Connection conn = DriverManager.getConnection(url, "root", "123123qwe");
            Statement stmt = conn.createStatement();
            String sqlStatement =
                    "CREATE DATABASE  IF NOT EXISTS `fredagsbar` /*!40100 DEFAULT CHARACTER SET utf8 */;\n" +
                            "USE `fredagsbar`;\n" +
                            "-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)\n" +
                            "--\n" +
                            "-- Host: localhost    Database: fredagsbar\n" +
                            "-- ------------------------------------------------------\n" +
                            "-- Server version\t5.7.9-log\n" +
                            "\n" +
                            "/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\n" +
                            "/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\n" +
                            "/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\n" +
                            "/*!40101 SET NAMES utf8 */;\n" +
                            "/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;\n" +
                            "/*!40103 SET TIME_ZONE='+00:00' */;\n" +
                            "/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;\n" +
                            "/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n" +
                            "/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;\n" +
                            "/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Table structure for table `attending`\n" +
                            "--\n" +
                            "\n" +
                            "DROP TABLE IF EXISTS `attending`;\n" +
                            "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n" +
                            "/*!40101 SET character_set_client = utf8 */;\n" +
                            "CREATE TABLE `attending` (\n" +
                            "  `fk_bar_name` varchar(25) NOT NULL,\n" +
                            "  `fk_user_email` char(20) NOT NULL,\n" +
                            "  `fk_datetime_from` datetime NOT NULL,\n" +
                            "  PRIMARY KEY (`fk_bar_name`,`fk_user_email`,`fk_datetime_from`),\n" +
                            "  KEY `fk_user_email_idx` (`fk_user_email`),\n" +
                            "  KEY `fk_bar_name_idx` (`fk_bar_name`),\n" +
                            "  KEY `fk_datetime_from` (`fk_datetime_from`),\n" +
                            "  CONSTRAINT `fk_bar_name` FOREIGN KEY (`fk_bar_name`) REFERENCES `barinfo` (`bar_name`) ON DELETE CASCADE ON UPDATE CASCADE,\n" +
                            "  CONSTRAINT `fk_datetime_from` FOREIGN KEY (`fk_datetime_from`) REFERENCES `events` (`event_datetime_from`) ON DELETE CASCADE ON UPDATE CASCADE,\n" +
                            "  CONSTRAINT `fk_user_email` FOREIGN KEY (`fk_user_email`) REFERENCES `users` (`user_email`) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
                            "/*!40101 SET character_set_client = @saved_cs_client */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Dumping data for table `attending`\n" +
                            "--\n" +
                            "\n" +
                            "LOCK TABLES `attending` WRITE;\n" +
                            "/*!40000 ALTER TABLE `attending` DISABLE KEYS */;\n" +
                            "INSERT INTO `attending` VALUES ('Kanalrundfartenenen','bjoe1381@stud.kea.dk','2015-12-01 01:30:00'),('Social Cluben','bjoe1381@stud.kea.dk','2015-12-07 14:49:57'),('Kanalrundfartenenen','bjoe1381@stud.kea.dk','2015-12-09 14:54:00');\n" +
                            "/*!40000 ALTER TABLE `attending` ENABLE KEYS */;\n" +
                            "UNLOCK TABLES;\n" +
                            "\n" +
                            "--\n" +
                            "-- Table structure for table `barfacilitet`\n" +
                            "--\n" +
                            "\n" +
                            "DROP TABLE IF EXISTS `barfacilitet`;\n" +
                            "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n" +
                            "/*!40101 SET character_set_client = utf8 */;\n" +
                            "CREATE TABLE `barfacilitet` (\n" +
                            "  `bar_name` varchar(20) NOT NULL,\n" +
                            "  `faci_name` varchar(45) NOT NULL,\n" +
                            "  PRIMARY KEY (`bar_name`,`faci_name`),\n" +
                            "  KEY `faci_name` (`faci_name`),\n" +
                            "  CONSTRAINT `bar_name` FOREIGN KEY (`bar_name`) REFERENCES `barinfo` (`bar_name`) ON DELETE CASCADE ON UPDATE CASCADE,\n" +
                            "  CONSTRAINT `faci_name` FOREIGN KEY (`faci_name`) REFERENCES `faciliteter` (`faci_name`) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
                            "/*!40101 SET character_set_client = @saved_cs_client */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Dumping data for table `barfacilitet`\n" +
                            "--\n" +
                            "\n" +
                            "LOCK TABLES `barfacilitet` WRITE;\n" +
                            "/*!40000 ALTER TABLE `barfacilitet` DISABLE KEYS */;\n" +
                            "INSERT INTO `barfacilitet` VALUES ('Kanalrundfartenenen','Beerpong'),('tacofest2000','Beerpong'),('Social Cluben','Bordfodbold'),('Kanalrundfartenenen','Bordtennis'),('tacofest2000','Bordtennis'),('Social Cluben','Udendørsareal'),('tacofest2000','Udendørsareal');\n" +
                            "/*!40000 ALTER TABLE `barfacilitet` ENABLE KEYS */;\n" +
                            "UNLOCK TABLES;\n" +
                            "\n" +
                            "--\n" +
                            "-- Table structure for table `barinfo`\n" +
                            "--\n" +
                            "\n" +
                            "DROP TABLE IF EXISTS `barinfo`;\n" +
                            "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n" +
                            "/*!40101 SET character_set_client = utf8 */;\n" +
                            "CREATE TABLE `barinfo` (\n" +
                            "  `bar_name` varchar(25) NOT NULL,\n" +
                            "  `bar_section` varchar(10) DEFAULT NULL,\n" +
                            "  `bar_info` text,\n" +
                            "  `bar_password` char(32) NOT NULL,\n" +
                            "  PRIMARY KEY (`bar_name`),\n" +
                            "  KEY `bar_section_idx` (`bar_section`),\n" +
                            "  CONSTRAINT `bar_section` FOREIGN KEY (`bar_section`) REFERENCES `sections` (`sec_name`) ON DELETE SET NULL ON UPDATE CASCADE\n" +
                            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
                            "/*!40101 SET character_set_client = @saved_cs_client */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Dumping data for table `barinfo`\n" +
                            "--\n" +
                            "\n" +
                            "LOCK TABLES `barinfo` WRITE;\n" +
                            "/*!40000 ALTER TABLE `barinfo` DISABLE KEYS */;\n" +
                            "INSERT INTO `barinfo` VALUES ('Kanalrundfartenenen','Design','Pris: 20kr','5f4dcc3b5aa765d61d8327deb882cf99'),('Social Cluben','Digital','hej','d41d8cd98f00b204e9800998ecf8427e'),('tacofest2000',NULL,'King kong taco','f869ce1c8414a264bb11e14a2c8850ed');\n" +
                            "/*!40000 ALTER TABLE `barinfo` ENABLE KEYS */;\n" +
                            "UNLOCK TABLES;\n" +
                            "\n" +
                            "--\n" +
                            "-- Table structure for table `events`\n" +
                            "--\n" +
                            "\n" +
                            "DROP TABLE IF EXISTS `events`;\n" +
                            "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n" +
                            "/*!40101 SET character_set_client = utf8 */;\n" +
                            "CREATE TABLE `events` (\n" +
                            "  `event_barname` varchar(25) NOT NULL,\n" +
                            "  `event_info` text,\n" +
                            "  `event_datetime_from` datetime NOT NULL,\n" +
                            "  `event_datetime_to` datetime DEFAULT NULL,\n" +
                            "  PRIMARY KEY (`event_datetime_from`,`event_barname`),\n" +
                            "  KEY `event_barname_idx` (`event_barname`),\n" +
                            "  CONSTRAINT `event_barname` FOREIGN KEY (`event_barname`) REFERENCES `barinfo` (`bar_name`) ON DELETE CASCADE ON UPDATE CASCADE\n" +
                            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
                            "/*!40101 SET character_set_client = @saved_cs_client */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Dumping data for table `events`\n" +
                            "--\n" +
                            "\n" +
                            "LOCK TABLES `events` WRITE;\n" +
                            "/*!40000 ALTER TABLE `events` DISABLE KEYS */;\n" +
                            "INSERT INTO `events` VALUES ('Kanalrundfartenenen','Hyggelygge','2015-12-01 01:30:00','2015-12-02 11:30:00'),('Kanalrundfartenenen','Phasellus ut tempus turpis. Sed ornare cursus luctus. Vestibulum non fermentum nisl. Cras in sem id enim imperdiet sollicitudin. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam sit amet elementum neque. Vivamus elementum eget sem a sodales. ...','2015-12-09 14:54:00','2015-11-11 00:00:00'),('Kanalrundfartenenen','JohnDoe aosijfasdf','2015-12-09 16:37:00','2015-12-02 00:00:00'),('Social Cluben','Huhej','2015-12-10 15:00:00','2015-11-24 12:12:00'),('Social Cluben','asdfagrhetag','2015-12-11 12:12:00','2015-12-27 11:11:00');\n" +
                            "/*!40000 ALTER TABLE `events` ENABLE KEYS */;\n" +
                            "UNLOCK TABLES;\n" +
                            "\n" +
                            "--\n" +
                            "-- Table structure for table `faciliteter`\n" +
                            "--\n" +
                            "\n" +
                            "DROP TABLE IF EXISTS `faciliteter`;\n" +
                            "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n" +
                            "/*!40101 SET character_set_client = utf8 */;\n" +
                            "CREATE TABLE `faciliteter` (\n" +
                            "  `faci_name` varchar(40) NOT NULL,\n" +
                            "  PRIMARY KEY (`faci_name`)\n" +
                            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
                            "/*!40101 SET character_set_client = @saved_cs_client */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Dumping data for table `faciliteter`\n" +
                            "--\n" +
                            "\n" +
                            "LOCK TABLES `faciliteter` WRITE;\n" +
                            "/*!40000 ALTER TABLE `faciliteter` DISABLE KEYS */;\n" +
                            "INSERT INTO `faciliteter` VALUES ('Beerpong'),('Bordfodbold'),('Bordtennis'),('Udendørsareal');\n" +
                            "/*!40000 ALTER TABLE `faciliteter` ENABLE KEYS */;\n" +
                            "UNLOCK TABLES;\n" +
                            "\n" +
                            "--\n" +
                            "-- Table structure for table `sections`\n" +
                            "--\n" +
                            "\n" +
                            "DROP TABLE IF EXISTS `sections`;\n" +
                            "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n" +
                            "/*!40101 SET character_set_client = utf8 */;\n" +
                            "CREATE TABLE `sections` (\n" +
                            "  `sec_name` varchar(20) NOT NULL,\n" +
                            "  `sec_address` varchar(40) NOT NULL,\n" +
                            "  PRIMARY KEY (`sec_name`)\n" +
                            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
                            "/*!40101 SET character_set_client = @saved_cs_client */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Dumping data for table `sections`\n" +
                            "--\n" +
                            "\n" +
                            "LOCK TABLES `sections` WRITE;\n" +
                            "/*!40000 ALTER TABLE `sections` DISABLE KEYS */;\n" +
                            "INSERT INTO `sections` VALUES ('Byg','Prinsesse Charlottes Gade 38'),('Design','Guldbergsgade 29N'),('Digital','Lygten 37'),('Teknik','Landskronagade 64');\n" +
                            "/*!40000 ALTER TABLE `sections` ENABLE KEYS */;\n" +
                            "UNLOCK TABLES;\n" +
                            "\n" +
                            "--\n" +
                            "-- Table structure for table `users`\n" +
                            "--\n" +
                            "\n" +
                            "DROP TABLE IF EXISTS `users`;\n" +
                            "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n" +
                            "/*!40101 SET character_set_client = utf8 */;\n" +
                            "CREATE TABLE `users` (\n" +
                            "  `user_name` varchar(50) DEFAULT NULL,\n" +
                            "  `user_email` char(20) NOT NULL,\n" +
                            "  `user_section` varchar(20) DEFAULT NULL,\n" +
                            "  `user_linje` varchar(40) DEFAULT NULL,\n" +
                            "  `user_rank` bit(1) DEFAULT NULL,\n" +
                            "  `user_password` char(32) NOT NULL,\n" +
                            "  PRIMARY KEY (`user_email`),\n" +
                            "  KEY `user_section_idx` (`user_section`),\n" +
                            "  CONSTRAINT `user_section` FOREIGN KEY (`user_section`) REFERENCES `sections` (`sec_name`) ON DELETE SET NULL ON UPDATE CASCADE\n" +
                            ") ENGINE=InnoDB DEFAULT CHARSET=utf8;\n" +
                            "/*!40101 SET character_set_client = @saved_cs_client */;\n" +
                            "\n" +
                            "--\n" +
                            "-- Dumping data for table `users`\n" +
                            "--\n" +
                            "\n" +
                            "LOCK TABLES `users` WRITE;\n" +
                            "/*!40000 ALTER TABLE `users` DISABLE KEYS */;\n" +
                            "INSERT INTO `users` VALUES ('John Doe','bjoe1381@stud.kea.dk','Digital','Datamatiker',NULL,'5f4dcc3b5aa765d61d8327deb882cf99'),('Johnny Reimer','bjoe1382@stud.kea.dk','Byg','Tekniker','\u0001','5f4dcc3b5aa765d61d8327deb882cf99'),('Kim Johan','John1234@stud.kea.dk','Design','Datamatiker',NULL,'5f4dcc3b5aa765d61d8327deb882cf99'),('Steffen Andersen','John1334@stud.kea.dk','Teknik','dat',NULL,'5f4dcc3b5aa765d61d8327deb882cf99'),('Lars Carstensen','John2234@stud.kea.dk',NULL,'Data','\u0001','5f4dcc3b5aa765d61d8327deb882cf99');\n" +
                            "/*!40000 ALTER TABLE `users` ENABLE KEYS */;\n" +
                            "UNLOCK TABLES;\n" +
                            "/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;\n" +
                            "\n" +
                            "/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;\n" +
                            "/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n" +
                            "/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;\n" +
                            "/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\n" +
                            "/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\n" +
                            "/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;\n" +
                            "/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;\n" +
                            "\n" +
                            "-- Dump completed on 2015-12-17  8:09:50\n";
            stmt.executeUpdate(sqlStatement);
        }
        catch (Exception e)
        {

        }

    }
}
