package Data;

import Controller.appMenuController;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.VBox;

import java.sql.*;
import java.util.*;
/**
 * Created by B on 08-12-2015.
 */
public class DB implements Connectable
{
    private static DB Instance = new DB();
    private static Connection conn;
    private PreparedStatement prepStmt;
    private Statement stmt;
    private ResultSet rs;

    private DB()
    {
        try
        {
            String url = "jdbc:mysql://localhost:3306/fredagsbar";
            conn = DriverManager.getConnection(url, "root", "123123qwe");
            stmt = conn.createStatement();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    public static DB getInstance()
    {
        return Instance;
    }
    //Udfører en Query der ikke er en SELECT
    public boolean executeQuery(String sqlString) throws SQLException
    {
        stmt.executeUpdate(sqlString);
        return true;
    }
    //Udfører en Query der ikke er en SELECT, med parametre
    public boolean executeQuery(String sqlString, Map<Integer, String> cmdParameters) throws SQLException
    {
        prepStmt = conn.prepareStatement(sqlString);
        for (Map.Entry<Integer, String> entry: cmdParameters.entrySet())
        {
            prepStmt.setString(entry.getKey(), entry.getValue());
        }
        prepStmt.executeUpdate();

        return true;
    }
    //Henter et query der ER en select
    public ResultSet getResult(String sqlString) throws SQLException
    {
        stmt.executeQuery(sqlString);
        rs = stmt.getResultSet();
        return rs;
    }
    //Henter et query der ER en select, med parametre
    public ResultSet getResult(String sqlString, Map<Integer, String> cmdParameters) throws SQLException
    {
        prepStmt = conn.prepareStatement(sqlString);
        for (Map.Entry<Integer, String> entry: cmdParameters.entrySet())
        {
            prepStmt.setString(entry.getKey(), entry.getValue());
        }
        prepStmt.executeQuery();
        return rs;
    }
    //Sætter data ind i en combobox
    public void populateComboBox(ComboBox<String> cbox, String colName, String tableName)
    {
        try
        {
            Instance.getResult("SELECT "+colName+" FROM "+tableName);
            while (rs.next())
            {
                cbox.getItems().add(rs.getString(colName));
            }
        }
        catch (SQLException e)
        {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
    //Ubrugt funktion
    public void emailSearch(ComboBox cbox,String email)
    {
        try
        {
            int size = cbox.getItems().size();
            for (int i = 0; i<size;i++)
            {
                cbox.getItems().remove(i);
            }

            Instance.getResult("SELECT user_email FROM users WHERE user_email LIKE '%"+email+"%'");
            while (rs.next())
            {
                cbox.getItems().add(rs.getString("user_email"));
            }
        }
        catch (SQLException e)
        {
            System.out.println("Error");
        }
    }
    //kan sikkert gøres bedre og er dum. Blivre kun brugt en enkelt gang fordi det er lavet dumt
    public static ResultSet secondaryResultSet(String sqlString)
    {
        try
        {
            Statement STMT = conn.createStatement();
            STMT.executeQuery(sqlString);
            return STMT.getResultSet();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
    //hakker faciliteter af i en VBox
    public void populateFacilities(VBox fac)
    {
        try
        {
            rs = DB.getInstance().getResult("SELECT * FROM barfacilitet WHERE bar_name ='" + JavaFXHelper.getBarSelected() +"'");
            while (rs.next())
            {
                for(Node c: fac.getChildren())
                {
                    CheckBox ch = (CheckBox) c;
                    if(ch.getText().equals(rs.getString("faci_name")))
                    {
                        ch.setSelected(true);
                    }
                }
            }
        }
        catch (Exception e)
        {

        }
    }
}
