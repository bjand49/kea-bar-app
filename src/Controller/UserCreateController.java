package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by B on 14-12-2015.
 */
public class UserCreateController implements Initializable
{
    @FXML
    ComboBox userEmail;
    @FXML
    CheckBox userRank;
    @FXML
    Label errorLabel;


    //Tilbage knap
    public void userBackButton()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
    //Forsøg på at lave noget søg i vælg bruger
    public void fillBox()
    {
        /*
        if(!userEmail.getEditor().getText().equals(""))
        {
            DB.getInstance().emailSearch(userEmail,userEmail.getEditor().getText());
        }*/
    }
    // Opdaterer en bruger til admin hvis han findes i databasen
    public void userUpdateButton()
    {
        if(userRank.isSelected())
        {
            try
            {
                DB.getInstance().executeQuery("UPDATE Users SET user_rank = 1 WHERE user_email='" +userEmail.getValue().toString()+"'");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            errorLabel.setText("Der gik noget galt!");
        }
    }

    //Putter data(brugere) i en combobox
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        DB.getInstance().populateComboBox(userEmail,"user_email","users");
    }
}
