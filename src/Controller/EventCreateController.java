package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import java.net.URL;
import java.sql.*;
import java.util.*;

public class EventCreateController implements Initializable
{
    @FXML
    TextField   eventTimeFrom, eventTimeTo;
    @FXML
    TextArea    eventInfo;
    @FXML
    DatePicker  eventDateFrom, eventDateTo;
    @FXML
    ComboBox    eventBar;
    @FXML
    Label       errorLabel;

    JavaFXHelper fxHelper = JavaFXHelper.getInstance();

    //Her hvis vi man er logget ind som en bar, vælger den selv baren i barComboboxen og disabler den
    //eller udfylder den comboboxen med alle barer fra databasen
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        if(JavaFXHelper.getBar() != null)
        {
            eventBar.setValue(JavaFXHelper.getBar().getBarName());
            eventBar.setDisable(true);
        }
        else
        {
            DB.getInstance().populateComboBox(eventBar,"bar_name","barinfo");
        }
    }
    // tjekker om de forskellige felter er tomme, og hvis de ikke er så indsætter den
    // et nyt event i tabellen
    public void eventButtonCreate() throws SQLException
    {
        if(fxHelper.checkControlEmpty(errorLabel,eventBar,eventTimeFrom,eventTimeTo,eventDateFrom,eventDateTo))
        {
            DB db = DB.getInstance();
            LinkedHashMap<Integer,String> lhm = new LinkedHashMap<>();
            lhm.put(1, eventBar.getValue().toString());
            lhm.put(2, eventInfo.getText());
            lhm.put(3, JavaFXHelper.getTime(eventDateFrom,eventTimeFrom));
            lhm.put(4, JavaFXHelper.getTime(eventDateTo,eventTimeTo));
            db.executeQuery("INSERT INTO events VALUES(?,?,?,?)",lhm);
            JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
        }
        else
        {
            System.out.println("nop");
        }
    }
    //Tilbage knap
    public void back()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
}