package Data;

public class User
{
    private String   userFullname;
    private String   userMail;
    private String   userSection;
    private String   userLine;
    private int      userRank;
    private String   userPassword;

    public User(String userFullname, String userMail, String userSection, String userLine, int userRank, String userPassword)
    {
        this.userFullname = userFullname;
        this.userMail = userMail;
        this.userSection = userSection;
        this.userLine = userLine;
        this.userRank = userRank;
        this.userPassword = userPassword;
    }
    public int getUserRank()
    {
        return userRank;
    }

    public String getUserFullname()
    {
        return userFullname;
    }

    public String getUserMail()
    {
        return userMail;
    }

    public String getUserSection()
    {
        return userSection;
    }

    public String getUserEducation()
    {
        return userLine;
    }
}