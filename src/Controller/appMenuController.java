package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.sql.*;
import java.util.ResourceBundle;

/**
 * Created by B on 03-12-2015.
 *
 */
public class appMenuController implements Initializable
{
    @FXML
    VBox barer,eventBox;
    @FXML
    Button opretBarBtn,opretEventBtn;
    @FXML
    TitledPane profil,fredagsbar,admin;
    @FXML
    Accordion wrapper;

    //Skifter scene når man trykker på min profil. Hvis man er logget ind som en bar
    //kommer man ind på barens profil, ellers kommer man ind på brugerens profil
    public void profil()
    {
        if(JavaFXHelper.getUser()!=null)
        {
            JavaFXHelper.getInstance().changeScene("../Gui/UserShow.fxml");
        }
        else
        {
            JavaFXHelper.setBarSelected(JavaFXHelper.getBar().getBarName());
            JavaFXHelper.getInstance().changeScene("../Gui/BarEdit.fxml");
        }
    }
    //Tjekker om man er logget ind. Hvis man ikke er logget ind, ryger man tilbage til login menu'en.
    //Derefter tjekker den hvilke rettigheder man har. Hvis man er logget som en normal bruger har man ikke adgang til det hele
    //hvorimod hvis man er logget ind som en admin har man adgang til det hele. Hvis du er logget ind som en bar
    // har du adgang til relevante bar funktioner.
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        if(JavaFXHelper.getUser()!=null || JavaFXHelper.getBar()!=null)
        {
            if (JavaFXHelper.getUser() != null)
            {
                if(JavaFXHelper.getUser().getUserRank()==0)
                {
                    barer.getChildren().remove(opretBarBtn);
                    eventBox.getChildren().remove(opretEventBtn);
                    wrapper.getPanes().remove(admin);
                }
            }
            else if(JavaFXHelper.getBar()!=null)
            {
                wrapper.getPanes().remove(fredagsbar);
                System.out.println("bar");
                wrapper.getPanes().remove(admin);
            }
            try
            {
                DB db = DB.getInstance();
                ResultSet result = db.getResult("SELECT bar_name FROM barinfo");
                while (result.next())
                {
                    Button btn = new Button(result.getString("bar_name"));
                    btn.setPrefWidth(300);
                    btn.setMinHeight(45);
                    btn.setOnAction(e -> {
                        if(JavaFXHelper.getUser()!=null)
                        {
                            if(JavaFXHelper.getUser().getUserRank()==0)
                            {
                                JavaFXHelper.getInstance().changeBarScene("../Gui/Barshow.fxml", btn.getText());
                            }
                            else
                            {
                                JavaFXHelper.getInstance().changeBarScene("../Gui/BarEdit.fxml", btn.getText());
                            }
                        }
                        else
                        {
                            JavaFXHelper.getInstance().changeBarScene("../Gui/BarEdit.fxml", btn.getText());
                        }
                    });
                    barer.getChildren().add(btn);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            JavaFXHelper.getInstance().changeScene("Gui/login.fxml");
        }
    }
    //Alt herunder skifter bare scene
    public void opretBar()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/BarCreate.fxml");
    }
    public void createEvents()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/EventCreate.fxml");
    }
    public void showEvents()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/EventIndex.fxml");
    }
    public void createSection()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/SectionCreate.fxml");
    }
    public void createUser()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/UserCreate.fxml");
    }
    public void createFac()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/FacilityCreate.fxml");
    }
}