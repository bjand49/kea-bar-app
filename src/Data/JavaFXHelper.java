package Data;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.security.MessageDigest;

/**
 * Created by B on 16-12-2015.
 */
public class JavaFXHelper
{
    private static JavaFXHelper Instance = new JavaFXHelper();
    private FXMLLoader fLoad;
    private static String barSelected;
    private static User user;
    private static Bar bar;

    public static JavaFXHelper getInstance()
    {
        return Instance;
    }

    private JavaFXHelper()
    {
        user = null;
        bar = null;
        barSelected = null;
    }
    //Skifter scene i programmet med et bar parameter
    public void changeBarScene(String fxml,String btnText)
    {
        barSelected = btnText;
        changeScene(fxml);
    }
    //Skifter scene i programmet
    public void changeScene(String fxml)
    {
        try
        {
            fLoad = new FXMLLoader(getClass().getResource(fxml));
            Parent root = fLoad.load();
            Stage stg = Main.window;
            stg.setScene(new Scene(root, 300, 500));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String getTime(DatePicker date, TextField time)
    {
        String str = time.getText();
        String splitChar = "%";
        int splitPos = 0;
        for (int i = 1; i<str.length()-1;i++)
        {
            switch (str.charAt(i))
            {
                case '.': splitChar = "\\" +str.charAt(i);
                    break;
                case ':': splitChar = "" +str.charAt(i);
                    break;
                case '-': splitChar = "" +str.charAt(i);
                    break;
            }
            if(!splitChar.equals("%"))
            {
                splitPos = i;
                break;
            }
        }
        if(splitPos != 0)
        {
            String values[] = str.split(splitChar);
            try
            {
                String hour = values[0];
                String min = values[1];
                if(values[0].length() < 2)
                {
                    System.out.println();
                    hour = "0"+values[0];
                }
                if(values[1].length() < 2)
                {
                    min = "0"+values[1];
                }
                String tid = hour+":"+min;
                String dato = date.getValue().toString();
                return dato+" "+tid;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        return "error";
    }
    public boolean checkControlEmpty(Label errorLabel, Control... cfa)
    {
        boolean mistake = true;
        boolean error;
        for (Control c:cfa)
        {
            error = false;
            c.setBorder(null);
            if (c instanceof TextField)
            {
                TextField t = (TextField) c;
                if (t.getText().equals(""))
                {
                    errorLabel.setText("Udfyld textfelt");
                    System.out.println("tom textfield");
                    error=true;
                    mistake = false;
                }
            }
            else if(c instanceof TextArea)
            {
                TextArea t = (TextArea) c;
                if (t.getText().equals(""))
                {

                    System.out.println("tom textarea");
                    error=true;
                    mistake = false;
                }
            }
            else if(c instanceof DatePicker)
            {
                DatePicker t = (DatePicker) c;
                if (t.getValue() == null)
                {
                    System.out.println("tom datepicker");
                    error=true;
                    mistake = false;
                }
            }
            else if(c instanceof ComboBox)
            {
                ComboBox t = (ComboBox) c;
                if (t.getValue() == null)
                {
                    System.out.println("tom comboBox");
                    error=true;
                    mistake = false;
                }
            }
            if(error)
            {
                c.setBorder(new Border(new BorderStroke(Color.RED, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT,new Insets(0))));
            }
        }
        return mistake;
    }
    public boolean validateEmail(String str)
    {
        try
        {
            //Kun for studerende, der er ikke tænkt på lærere eller noget, evt opfølge med register over keamails
            if(str.substring(str.length() - 12, str.length()).equals("@stud.kea.dk") && str.length() == 20)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }
    }
    public String encrypt(String pass)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((pass).getBytes());
            byte byteData[] = md.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < byteData.length; i++)
            {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }
        catch (Exception e)
        {

        }
        return "error";
    }
    public static String getBarSelected()
    {
        return barSelected;
    }
    public static void setBarSelected(String barSelected)
    {
        JavaFXHelper.barSelected = barSelected;
    }
    public static User getUser()
    {
        return user;
    }
    public static void setUser(User user)
    {
        JavaFXHelper.user = user;
    }
    public static Bar getBar()
    {
        return bar;
    }
    public static void setBar(Bar bar)
    {
        JavaFXHelper.bar = bar;
    }
}
