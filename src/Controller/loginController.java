package Controller;

import Data.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;

public class loginController implements Initializable
{
    @FXML
    TextField cUserMail, cUserName, lUserName,lPass;
    @FXML
    ComboBox cUserSection, cLinje;
    @FXML
    VBox PaneGlemt, PaneOpret, PaneLogin;
    @FXML
    Hyperlink hlOpret, hlGlemt;
    @FXML
    Button btnBack;
    @FXML
    Label ErrorLabel;
    //Viser panet opret bruger
    public void visOpret()
    {
        PaneGlemt.setVisible(false);
        PaneOpret.setVisible(true);
        PaneLogin.setVisible(false);
        btnBack.setVisible(true);
    }
    //Viser panet glemt bruger
    public void visGlemt()
    {
        PaneGlemt.setVisible(true);
        PaneOpret.setVisible(false);
        PaneLogin.setVisible(false);
        btnBack.setVisible(true);
    }
    //Viser panet login
    public void visLogin()
    {
        PaneGlemt.setVisible(false);
        PaneOpret.setVisible(false);
        PaneLogin.setVisible(true);
        btnBack.setVisible(false);
    }
    //Tjekker om de forkellige controls er tomme
    //hvis de ikke er så tjekker vi om mailen er gyldig
    //hvis den er det, så tjekker vi om password og kode stemmer overens
    //hvis de gør det, så logger vi ind og kommmer hen til menuen
    //Hvis de ikke stemmer overens skriver vi ud  at der var en fejl i en label
    //Hvis det ikke er en gyldig email, så tjekker vi login for en bar istedet for på samme måde som når vi tjekker for en bruger
    //
    public void login()
    {
        JavaFXHelper fxHelper = JavaFXHelper.getInstance();
        if(fxHelper.checkControlEmpty(ErrorLabel,lUserName,lPass))
        {
            try
            {
                if(fxHelper.validateEmail(lUserName.getText()))
                {
                    ResultSet rs = DB.getInstance().getResult("SELECT * FROM users WHERE user_email = '" + lUserName.getText() + "' AND user_password='" + fxHelper.encrypt(lPass.getText()) + "'");
                    if(rs.next())
                    {
                        JavaFXHelper.setUser(new User(rs.getString("user_name"),rs.getString("user_email"),rs.getString("user_section"),rs.getString("user_linje"),rs.getInt("user_rank"),rs.getString("user_password")));
                        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
                    }
                    ErrorLabel.setText("Forket kode/password");
                }
                else
                {
                    ResultSet rs = DB.getInstance().getResult("SELECT * FROM barinfo WHERE bar_name = '" + lUserName.getText() + "' AND bar_password='" + fxHelper.encrypt(lPass.getText()) + "'");
                    if(rs.next())
                    {
                        JavaFXHelper.setBar(new Bar(rs.getString("bar_name"),rs.getString("bar_section"),rs.getString("bar_info"),rs.getString("bar_password")));
                        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
                    }
                    ErrorLabel.setText("Forket kode/password");
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }


    }
    //Tjekker om de forkellige controls er tomme og om emailen er valid
    //hvis de er det så tager vi daten og indsætter i databasen, med et password der hedder password
    //ellers hvis emailen er forkert så udskriver vi at man skal skrive en gyldig email
    public void userButtonCreate() throws SQLException
    {
        JavaFXHelper fxHelper = JavaFXHelper.getInstance();
        if(fxHelper.checkControlEmpty(ErrorLabel,cUserMail,cUserName,cUserSection) && fxHelper.validateEmail(cUserMail.getText()))
        {
            System.out.println("true");
            LinkedHashMap<Integer,String> lhm = new LinkedHashMap<>();
            lhm.put(1, cUserName.getText());
            lhm.put(2, cUserMail.getText());
            lhm.put(3, cUserSection.getValue().toString());
            lhm.put(4, null);
            lhm.put(5, fxHelper.encrypt("password")); // TODO: 15-12-2015 Mailserver
            DB.getInstance().executeQuery("INSERT INTO Users VALUES(?,?,?,?,NULL,?)",lhm);
            cUserMail.setText("");
            cUserName.setText("");
            ErrorLabel.setText("En mail er sendt til dig med kodeordet!(koden er 'password')");
        }
        else if(!fxHelper.validateEmail(cUserMail.getText()))
        {
            ErrorLabel.setText("Skriv en gyldig KEA mail");
        }
    }
    //Det eneste relevante der sker her er at vi udfylder dataen i comboboxen i opret bruger
    //resten er at den autoudfylder password og koden i de forskellige fields, samt laver noget dummy data i en deaktiveret combobox
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        cLinje.getItems().addAll("Datamatiker","SmykkeDesigner","BygningsMand");
        cLinje.setDisable(true);
        DB.getInstance().populateComboBox(cUserSection,"sec_name","sections");
        lUserName.setText("bananana@stud.kea.dk");
        lPass.setText("password");
    }

}
