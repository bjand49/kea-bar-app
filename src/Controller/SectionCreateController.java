package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 * Created by B on 14-12-2015.
 */

public class SectionCreateController implements Initializable
{
    @FXML
    VBox sections;
    @FXML
    TextField secTxt,secAdd;
    @FXML
    Label errorLabel;
    //fylder VBox'en ud med forskellige sektioner
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            ResultSet rs = DB.getInstance().getResult("SELECT sec_name FROM sections");
            while (rs.next())
            {
                HBox hBox = new HBox();
                String str = rs.getString("sec_name");
                Label lbl = new Label(str);
                Button btn = new Button("Slet");
                lbl.setMinWidth(160);
                btn.setMinWidth(40);
                btn.setOnAction(e->delete(hBox,str));
                hBox.getChildren().addAll(lbl,btn);
                sections.getChildren().add(hBox);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    //Tjekker om de forskellige relevante controls er tomme.
    //Hvis de ikke er så indsætter den en sektion i databasen og i VBox'en
    public void createSection()// TODO: 15-12-2015 make me nice
    {
        try
        {
            JavaFXHelper fxHelper = JavaFXHelper.getInstance();

            if(fxHelper.checkControlEmpty(errorLabel,secTxt,secAdd))
            {
                try
                {
                    DB.getInstance().executeQuery("INSERT INTO sections VALUES('" + secTxt.getText() + "','" + secAdd.getText() + "')");
                }
                catch (Exception ex)
                {
                    throw new SQLException("Navn findes allerede");
                }

                HBox hBox = new HBox();
                String str = secTxt.getText();
                Label lbl = new Label(str);
                Button btn = new Button("Slet");
                btn.setOnAction(e -> delete(hBox,str));
                lbl.setMinWidth(160);
                btn.setMinWidth(40);
                hBox.getChildren().addAll(lbl, btn);
                sections.getChildren().add(hBox);
                errorLabel.setText("");
            }
        }
        catch (SQLException e)
        {
            errorLabel.setText("Sektion findes allerede");

        }
    }
    //Tilbage knap
    public void back()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
    //Funktion der bliver brugt til at slette sektioner i databasen
    private void delete(HBox hBox, String str)
    {
        try
        {
            DB.getInstance().executeQuery("DELETE FROM sections WHERE sec_name='"+str+"'");
        }
        catch (SQLException e)
        {
            errorLabel.setText("Fejl, det slettede element findes ikke");
            e.printStackTrace();
        }
        sections.getChildren().remove(hBox);
    }
}
