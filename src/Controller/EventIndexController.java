package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

/**
 * Created by B on 09-12-2015.
 */
public class EventIndexController implements Initializable
{
    protected static String[] event = new String[2];
    @FXML
    VBox eventContainer;
    @Override
    //Der sker rigtigt meget her, så jeg skriver kommentarer i små bidder inde i metoden
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            // Tjekker om vi er logget ind som en bar. hvis vi er det så skal vi kun kunne se de events som baren selv står fo
            // ellers viser den alle events

            ResultSet rs;
            if(JavaFXHelper.getBar()!=null)
            {
                rs = DB.getInstance().getResult("SELECT * FROM events WHERE event_barname='"+JavaFXHelper.getBar().getBarName()+"'");
            }
            else
            {
                rs = DB.getInstance().getResult(
                        "SELECT events.*,barinfo.bar_section " +
                        "FROM events " +
                        "LEFT JOIN barinfo " +
                        "ON events.event_barname=barinfo.bar_name");
            }
            //Her udskriver den alle de events der er. Den udskriver dem i gridpanes og tilføjer daten i hvert gridpane
            while (rs.next())
            {
                GridPane gPane = new GridPane();
                gPane.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(5), BorderWidths.DEFAULT,new Insets(0,0,0,0))));
                Label bNavn = new Label(rs.getString("event_barname"));
                Label sNavn = new Label();
                try
                {
                    sNavn.setText(rs.getString("bar_section"));
                }
                catch (Exception e)
                {

                }
                //Formaterer tiden så den ser ordenlig ud
                SimpleDateFormat prut = new SimpleDateFormat("EEEE, dd. MMMM");
                Timestamp datefrom = rs.getTimestamp("event_datetime_from");
                Button attBtn = new Button("Tilmeld");
                Button btn = new Button("Rediger");
                btn.setPrefWidth(75);
                attBtn.setPrefWidth(75);

                //Her laver vi et popup vindue så vi ikke kommer til at slette et event ved en fejltagelse
                //Og den sletter kun hvis man trykker ja
                Hyperlink del = new Hyperlink("Slet");
                del.setOnAction(e->{

                    Stage popUpStage = new Stage();
                    HBox hbox = new HBox();
                    Button y = new Button("Ja");
                    Button n = new Button("Nej");
                    n.setOnAction(event->{
                        popUpStage.close();
                    });
                    y.setOnAction(event->{
                        eventContainer.getChildren().remove(gPane);
                        System.out.println("DELETE FROM events WHERE event_barname='"+bNavn.getText()+"' AND event_datetime_from='"+datefrom+"'");
                        try
                        {
                            DB.getInstance().executeQuery("DELETE FROM events WHERE event_barname='"+bNavn.getText()+"' AND event_datetime_from='"+datefrom+"'");
                        }
                        catch (SQLException ex)
                        {
                            ex.printStackTrace();
                        }
                        popUpStage.close();
                    });
                    hbox.getChildren().addAll(y,n);
                    Scene myScene = new Scene(hbox, 300, 100);
                    popUpStage.setScene(myScene);
                    popUpStage.initModality(Modality.APPLICATION_MODAL);
                    popUpStage.setTitle("Confirm delete?");
                    popUpStage.showAndWait();
                });
                //Gemmer primarykey'en her for at kunne få fat i det rigtige event når vi går hen til event edit
                btn.setOnAction(e->{
                    event[0] = bNavn.getText();
                    event[1] = datefrom.toString();
                    JavaFXHelper.getInstance().changeScene("../Gui/EventEdit.fxml");
                });

                //Hvis man er logget ind så skal man have mulighed for at kunne tilmelde og afmelde sig events.
                //Hvis man er admin skal sletteknappen og rediger knappen
                if(JavaFXHelper.getUser() != null)
                {
                    String dumbAssStupidString =
                            "SELECT fk_user_email " +
                                "FROM attending " +
                                "WHERE fk_bar_name='"+rs.getString("event_barname")+
                                "' AND fk_user_email='"+JavaFXHelper.getUser().getUserMail()+
                                " 'AND fk_datetime_from = '"+datefrom+"'";
                    ResultSet rsAtt = DB.secondaryResultSet(dumbAssStupidString);

                    if(rsAtt.next())
                    {
                        attBtn.setText("Frameld");
                    }
                    attBtn.setOnAction(e->{
                        try
                        {
                            if(attBtn.getText().equals("Tilmeld"))
                            {
                                DB.getInstance().executeQuery("INSERT INTO attending VALUES('"+bNavn.getText()+"','"+JavaFXHelper.getUser().getUserMail()+"','"+datefrom+"')");
                                attBtn.setText("Frameld");
                            }
                            else
                            {
                                DB.getInstance().executeQuery("DELETE FROM attending WHERE fk_bar_name='"+bNavn.getText()+"' AND fk_user_email='"+JavaFXHelper.getUser().getUserMail()+"' AND fk_datetime_from='"+datefrom+"'");
                                attBtn.setText("Tilmeld");
                            }
                        }
                        catch (SQLException ex)
                        {
                            ex.printStackTrace();
                        }
                    });
                    gPane.add(attBtn,1,3);
                    if(JavaFXHelper.getUser().getUserRank()!=0)
                    {
                        gPane.add(del,1,0);
                        gPane.add(btn,0,3);

                    }
                }
                //hvis du er logget ind som en bar skal du også kunne rette eller slette det.
                else if(JavaFXHelper.getBar()!=null)
                {
                    gPane.add(del,1,0);
                    gPane.add(btn,0,3);
                }


                attBtn.setPrefWidth(75);
                Label date = new Label(prut.format(datefrom));
                gPane.add(bNavn,0,0);
                gPane.add(sNavn,0,1);
                gPane.add(date,0,2,2,1);


                gPane.setHgap(20);
                gPane.setVgap(10);
                gPane.setMinWidth(200);
                gPane.setPrefWidth(200);
                gPane.setMaxWidth(200);
                gPane.setPadding(new Insets(10,10,10,10));
                eventContainer.getChildren().add(gPane);

                //Viser et evennt
                gPane.setOnMouseClicked(e-> {
                    event[0] = bNavn.getText();
                    event[1] = datefrom.toString();
                    JavaFXHelper.getInstance().changeScene("../Gui/VisEtEvent.fxml");
                });
                //Laver noget visuelt så man kan se at man kan klikke på de forskellige events
                gPane.setOnMouseEntered(e->{
                    gPane.setStyle("-fx-background-color: lightgray;-fx-cursor: hand;");
                });
                gPane.setOnMouseExited(e->{
                    gPane.setStyle("-fx-background-color: #f4f4f4");
                });
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String[] getEvent()
    {
        return event;
    }
    //Tilbage knap
    public void back()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
}