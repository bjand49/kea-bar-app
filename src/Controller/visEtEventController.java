package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

/**
 * Created by B on 10-12-2015.
 */
public class visEtEventController implements Initializable
{
    @FXML
    Label eventDateFrom, eventDateTo, eventInfo, eventNavn ,eventSection, eventAddress;

    //Henter data ud fra databasen og indsætter på de relevante controls.
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            System.out.println(EventIndexController.getEvent()[0]);
            System.out.println(EventIndexController.getEvent()[1]);
            ResultSet rs = DB.getInstance().getResult(
                    "SELECT event_barname, event_info, event_datetime_from, event_datetime_to, sec_address, bar_section " +
                    "FROM Barinfo " +
                    "LEFT JOIN Sections ON Sections.sec_name = Barinfo.bar_section " +
                    "JOIN Events ON events.event_barname = barinfo.bar_name " +
                    "WHERE event_barname = '" + EventIndexController.getEvent()[0] +"' AND event_datetime_from ='" + EventIndexController.getEvent()[1] +"'");
            while (rs.next())
            {
                eventNavn.setText(rs.getString("event_barname"));
                eventDateFrom.setText(rs.getDate("event_datetime_from").toString());
                eventDateTo.setText(rs.getDate("event_datetime_to").toString());
                eventSection.setText(rs.getString("bar_section"));
                eventInfo.setText(rs.getString("event_info"));
                eventAddress.setText(rs.getString("sec_address"));
            }
        }
        catch (Exception e)
        {
            System.out.println("ey");
            e.printStackTrace();
        }

    }
    //Tilbage knap
    public void back()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
}
