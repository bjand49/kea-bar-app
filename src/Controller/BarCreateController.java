package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.sql.ResultSet;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 * Created by B on 09-12-2015.
 */
    public class BarCreateController implements Initializable
    {
    @FXML
    ComboBox<String> CBox;
    @FXML
    TextField BNavn;
    @FXML
    Label ErrorLabel;
    @FXML
    TextArea BInfo;
    @FXML
    VBox faciBox;
    @FXML
    PasswordField pass1,pass2;
    //Putter data i combobox og skriver faciliteter ud
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        DB.getInstance().populateComboBox(CBox,"sec_name","sections");
        try
        {
            ResultSet rs = DB.getInstance().getResult("SELECT * FROM faciliteter");
            while (rs.next())
            {
                CheckBox chcbox = new CheckBox(rs.getString("faci_name"));
                faciBox.getChildren().add(chcbox);
            }
        }
        catch (Exception e)
        {

        }
    }
    // Prøver at indsætte en bar i databasen, men tjekker om den findes først
    public void save()
    {
        ErrorLabel.setText("");
        JavaFXHelper fxHelper = JavaFXHelper.getInstance();
        if (fxHelper.checkControlEmpty(ErrorLabel, CBox, BNavn, BInfo,pass1,pass2) && pass1.getText().equals(pass2.getText()))
        {
            Map<Integer, String> m = new TreeMap<>();
            m.put(1, BNavn.getText());
            m.put(2, CBox.getValue());
            m.put(3, BInfo.getText());
            m.put(4, fxHelper.encrypt(pass1.getText()));
            DB db = DB.getInstance();
            boolean exist = false;
            try
            {

                ResultSet rs = db.getResult("SELECT bar_name FROM barinfo WHERE bar_name = '" + BNavn.getText() + "'");
                System.out.println(BNavn.getText());
                while (rs.next())
                {
                    if (rs.getString("bar_name").equals(BNavn.getText()))
                    {
                        exist = true;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                System.out.println("sum error");
                e.printStackTrace();
            }
            if (!exist)
            {
                try
                {
                    db.executeQuery("INSERT INTO barinfo VALUES(?,?,?,?)", m);
                    for (Node c : faciBox.getChildren())
                    {
                        CheckBox ch = (CheckBox) c;
                        if (ch.isSelected())
                        {
                            db.executeQuery("INSERT INTO barfacilitet VALUES('" + BNavn.getText() + "','" + ch.getText() + "')");
                        }
                    }
                    JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            } else
            {
                ErrorLabel.setText("Navn findes allerede");
            }
        }
    }
    //Går tilbage
    public void back()
        {
            JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
        }
}
