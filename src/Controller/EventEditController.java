package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by B on 09-12-2015.
 */
public class EventEditController implements Initializable
{
    @FXML
    TextField   eventTimeFromEdit, eventTimeToEdit;
    @FXML
    TextArea    eventInfoEdit;
    @FXML
    DatePicker  eventDateFromEdit, eventDateToEdit;
    @FXML
    ComboBox    eventBarEdit;
    @FXML
    Button      eventBackEdit, eventSaveEdit;
    @FXML
    Label       errorLabel;
    //Tilbage knap
    public void eventBackEdit()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
    //Tjekker om de forskellige controls er tomme, hvis de ikke er så gør den ikke noget
    //hvis den er, så updaterer den det event du er inde på og går tilbage til eventindex
    public void eventSaveEdit()
    {
        try
        {
            JavaFXHelper fxHelper = JavaFXHelper.getInstance();
            if(fxHelper.checkControlEmpty(errorLabel,eventBarEdit,eventDateFromEdit,eventDateToEdit,eventTimeFromEdit, eventTimeToEdit))
            {
                Map<Integer, String> m = new HashMap();
                m.put(1,    eventBarEdit.getValue().toString());
                m.put(2,    JavaFXHelper.getTime(eventDateFromEdit, eventTimeFromEdit));
                m.put(3,    JavaFXHelper.getTime(eventDateToEdit, eventTimeToEdit));
                m.put(4,    eventInfoEdit.getText());
                DB.getInstance().executeQuery("UPDATE events SET event_barname=?, event_datetime_from=?, event_datetime_to=?, event_info=? WHERE event_barname='"+EventIndexController.getEvent()[0] + "' AND event_datetime_from = '" + EventIndexController.getEvent()[1] +"'",m);
                JavaFXHelper.getInstance().changeScene("../Gui/EventIndex.fxml");
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    //Indsætter de forskellige data fra databasen ind i controllerene ud fra hvilket event der er valgt
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        DB.getInstance().populateComboBox(eventBarEdit,"bar_name","barinfo");
        try
        {
            ResultSet rs = DB.getInstance().getResult("SELECT * FROM events WHERE event_barname = '" + EventIndexController.getEvent()[0] +"' AND event_datetime_from ='" + EventIndexController.getEvent()[1] +"'");
            while(rs.next())
            {
                Timestamp timeFrom  = rs.getTimestamp("event_datetime_from");
                Timestamp timeTo    = rs.getTimestamp("event_datetime_to");

                eventBarEdit.setValue(rs.getString("event_barname"));
                eventDateFromEdit.setValue(rs.getDate("event_datetime_from").toLocalDate());
                eventTimeFromEdit.setText(timeFrom.toString().substring(11, 16));
                eventDateToEdit.setValue(rs.getDate("event_datetime_to").toLocalDate());
                eventTimeToEdit.setText(timeTo.toString().substring(11, 16));
                eventInfoEdit.setText(rs.getString("event_info"));

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
