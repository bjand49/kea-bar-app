package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

/**
 * Created by B on 09-12-2015.
 */
public class BarShowController implements Initializable
{
    @FXML
    Label barName, barSection, barInfo, barAddress;
    @FXML
    VBox vboxFac;

    ResultSet rs;
    //Tilbage knap
    public void backShow()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }

    //Henter texten ind fra den valgte bar og skriver det ind i textfieldene
    // og sætter checkboxe ind og hakker dem af alt efter om de er i den valgte bar
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            ResultSet result = DB.getInstance().getResult("SELECT bar_name, bar_section, bar_info, sec_address FROM Barinfo LEFT JOIN Sections ON Sections.sec_name = Barinfo.bar_section WHERE bar_name ='" + JavaFXHelper.getBarSelected() + "'");
            while(result.next())
            {
                barName.setText(result.getString("bar_name"));
                barSection.setText(result.getString("bar_section"));
                barInfo.setText(result.getString("bar_info"));
                barAddress.setText(result.getString("sec_address"));
            }
            rs = DB.getInstance().getResult("SELECT * FROM faciliteter");
            while (rs.next())
            {
                CheckBox chcbox = new CheckBox(rs.getString("faci_name"));
                chcbox.setDisable(true);
                vboxFac.getChildren().add(chcbox);
            }
            DB.getInstance().populateFacilities(vboxFac);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}