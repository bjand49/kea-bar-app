package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.ResourceBundle;

public class UserShowController implements Initializable
{
    @FXML
    Label PassLabel, PassLabelRepeat;
    @FXML
    Button userEdit, userSave, userBack;
    @FXML
    TextField userFullnameEdit, userMailEdit, userEducationEdit;
    @FXML
    PasswordField userPasswordEdit, userPasswordRepeatEdit;
    @FXML
    ComboBox userSectionEdit;

    //Deaktiverer og aktiverer en masse controls så vi kan redigerer i brugerens data
    public void userEditButton()
    {
        userSave.setVisible(true);
        userEdit.setVisible(false);


        PassLabel.setVisible(true);
        userFullnameEdit.setDisable(false);
        userMailEdit.setVisible(true);
        userSectionEdit.setDisable(false);
        userEducationEdit.setDisable(false);
        userPasswordEdit.setVisible(true);
        userPasswordRepeatEdit.setVisible(true);
        PassLabelRepeat.setVisible(true);


    }
    //Tjekker om de 2 passwords der er skrevet er en og om de relevante controls er tomme
    //Hvis de ikke er det, så opdaterer vi brugerens data i databasen.
    public void userSaveButton()
    {
        JavaFXHelper fxHelper = JavaFXHelper.getInstance();
        if(userPasswordEdit.getText().equals(userPasswordRepeatEdit.getText()) && fxHelper.checkControlEmpty(new Label(),userFullnameEdit,userSectionEdit,userEducationEdit,userPasswordEdit))
        {
            try
            {
                DB db = DB.getInstance();
                LinkedHashMap<Integer, String> lhm = new LinkedHashMap<>();
                lhm.put(1, userFullnameEdit.getText());
                lhm.put(2, userSectionEdit.getValue().toString());
                lhm.put(3, userEducationEdit.getText());
                lhm.put(4, fxHelper.encrypt(userPasswordEdit.getText()));
                lhm.put(5, JavaFXHelper.getUser().getUserMail());
                db.executeQuery("UPDATE Users SET user_name = ?, user_section = ?, user_linje = ?, user_password = ? WHERE user_email = ?", lhm);
                userEdit.setVisible(true);
                userSave.setVisible(false);
                userFullnameEdit.setDisable(true);
                userMailEdit.setVisible(true);
                userSectionEdit.setDisable(true);
                userEducationEdit.setDisable(true);
                userPasswordEdit.setVisible(false);
                PassLabel.setVisible(false);
                userPasswordRepeatEdit.setVisible(false);
                PassLabelRepeat.setVisible(false);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(!userPasswordEdit.getText().equals(userPasswordRepeatEdit.getText()))
        {
            System.out.println("Passwords stemmer ikke overens!");
            System.out.println(userPasswordEdit.getText() + " " + userPasswordRepeatEdit.getText());
        }
    }
    //Sætter data(sektioner) ind i en combobox, og udfylder noget text i de relevante controls
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        try
        {
            DB.getInstance().populateComboBox(userSectionEdit,"sec_name","sections");
            System.out.println(JavaFXHelper.getUser().getUserSection());
            userFullnameEdit.setText(JavaFXHelper.getUser().getUserFullname());
            userMailEdit.setText(JavaFXHelper.getUser().getUserMail());
            userSectionEdit.setValue(JavaFXHelper.getUser().getUserSection());
            userEducationEdit.setText(JavaFXHelper.getUser().getUserEducation());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    //tilbage knap
    public void userBack()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
}