package Controller;

import Data.DB;
import Data.JavaFXHelper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import java.net.URL;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * Created by B on 09-12-2015.
 */
public class BarEditController implements Initializable
{
    @FXML
    VBox vboxFac;
    @FXML
    TextField textNavnEdit;
    @FXML
    TextArea areaInfoEdit;
    @FXML
    Label errorLabel;
    @FXML
    ComboBox comboBarEdit;
    @FXML
    PasswordField pass1,pass2;

    ResultSet rs;

    //Sætter dataen fra den valgte bar ind i de controls der er i de forskellige controls
    //samt udfylder data i comboboxen og indsætter de forskellige faciliteter
    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        DB.getInstance().populateComboBox(comboBarEdit,"sec_name","sections");
        try
        {
            rs = DB.getInstance().getResult("SELECT bar_name, bar_info, bar_section FROM Barinfo WHERE bar_name= '" + JavaFXHelper.getBarSelected() +"'");
            while (rs.next())
            {
                textNavnEdit.setText(rs.getString("bar_name"));
                areaInfoEdit.setText(rs.getString("bar_info"));
                comboBarEdit.setValue(rs.getString("bar_section"));
            }
            rs = DB.getInstance().getResult("SELECT * FROM faciliteter");
            while (rs.next())
            {
                CheckBox chcbox = new CheckBox(rs.getString("faci_name"));
                vboxFac.getChildren().add(chcbox);
            }
            DB.getInstance().populateFacilities(vboxFac);
        }
        catch(Exception e)
        {
            System.out.println("Error");
            e.printStackTrace();
        }
    }
    //Tilbage knap
    public void backEdit()
    {
        JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
    }
    //Prøver at gemme den ændrede bar.
    //Tjekker om alle de relevante controls er tomme, og sætter derefter deres værdier in i et map.
    //Derefter gemmer den det hele oven på den gamle bar's række i databasen, samt opdaterer relationen mellem facilites og baren
    public void saveEdit()
    {
        JavaFXHelper fxHelper = JavaFXHelper.getInstance();

        if(fxHelper.checkControlEmpty(errorLabel,textNavnEdit,comboBarEdit,areaInfoEdit) && pass1.getText().equals(pass2.getText()))
        {
            Map<Integer, String> m = new HashMap<>();
            m.put(1, textNavnEdit.getText());
            m.put(2, comboBarEdit.getValue().toString());
            m.put(3, areaInfoEdit.getText());
            m.put(4, fxHelper.encrypt(pass1.getText()));


            DB db = DB.getInstance();
            String oldBarName;
            try
            {
                if (JavaFXHelper.getBar()!=null)
                {
                    oldBarName = JavaFXHelper.getBar().getBarName();
                    JavaFXHelper.getBar().setBarName(textNavnEdit.getText());

                }
                else
                {
                    oldBarName = JavaFXHelper.getBarSelected();
                }
                m.put(5, oldBarName);
                db.executeQuery("UPDATE Barinfo SET bar_name=?, bar_section=?, bar_info=?, bar_password =? WHERE bar_name=?", m);
                System.out.println(oldBarName);
                db.executeQuery("DELETE FROM barfacilitet WHERE bar_name='"+textNavnEdit.getText()+"'");

                for(Node c: vboxFac.getChildren())
                {
                    CheckBox ch = (CheckBox) c;
                    if(ch.isSelected())
                    {
                        System.out.println(textNavnEdit.getText());
                        db.executeQuery("INSERT INTO barfacilitet VALUES('"+textNavnEdit.getText()+"','"+ch.getText()+"')");
                    }
                }
                JavaFXHelper.getInstance().changeScene("../Gui/AppMenu.fxml");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        else if(!pass1.getText().equals(pass2.getText()))
        {
            errorLabel.setText("Password stemmer ikke overens");
        }
    }
}
