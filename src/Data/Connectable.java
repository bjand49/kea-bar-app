package Data;

import javafx.scene.control.ComboBox;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**this is a fucking joke,
* vi har ikke noget sted hvor vi føler at inheritance eller interfaces har været brugbare
* så vi har valgt at noget ligegyldigt interface her så vi i det mindste har vist at vi
*kan finde ud af at implentere det i vores kode.
* */

public interface Connectable
{
    boolean executeQuery(String sqlString) throws SQLException;
    boolean executeQuery(String sqlString, Map<Integer, String> cmdParameters) throws SQLException;
    ResultSet getResult(String sqlString) throws SQLException;
    ResultSet getResult(String sqlString, Map<Integer, String> cmdParameters) throws SQLException;
    void populateComboBox(ComboBox<String> cbox,String colName, String tableName);
}