package Data;
/**
 * Created by noxtydk on 20/11/15.
 */
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application
{
    public static Stage window;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        window = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("../Gui/login.fxml"));
        primaryStage.setTitle("Bar lan");
        primaryStage.setScene(new Scene(root, 300, 500));
        primaryStage.show();
    }
}