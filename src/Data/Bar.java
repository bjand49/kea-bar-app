package Data;

/**
 * Created by noxtydk on 10/12/15.
 */
public class Bar
{
    private String  barName;
    private String  barSection;
    private String  barInfo;
    private String  barPassword;

    public Bar(){}
    public Bar(String barName, String barSection, String barInfo, String barPassword)
    {
        this.barName = barName;
        this.barSection = barSection;
        this.barInfo = barInfo;
        this.barPassword = barPassword;
    }

    public String getBarName()
    {
        return barName;
    }

    public void setBarName(String barName)
    {
        this.barName = barName;
    }
}
